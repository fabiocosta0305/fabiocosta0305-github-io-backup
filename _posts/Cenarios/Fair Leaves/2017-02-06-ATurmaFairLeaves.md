---
title: A Turma
teaser: "O múcleo de únião de todos, a primeira e mais importante ligação entre todos"
layout: post
date: 2017-02-06 16:00:00 -0200
categories: 
    - RPG
tags:
    - Fate
    - FAE
    - Personagens
    - Arquétipos
    - Playsheets
    - Fair Leaves
    - Light Hearted
    - Fair-Leaves
header: no
---

A Turma é a primeira ligação que todos possuem com pessoas de fora de sua família, desconsiderando-se as ligações de autoridade. Em uma Turma, todos são iguais em direitos, deveres e responsabilidades. As idades podem ser levemente diferentes, mas todos interagem mais ou menos de maneira igual.

Apesar disso, dessa aparente igualdade, as pessoas vão percebendo que são diferentes entre si: algumas possuem talentos que outras não possuem, ideias que as demais não compartilham e opiniões  com as quais elas não discordam. Isso pode levar a pequenas desavenças, desentendimentos e complicações. 

Mas para os integrantes da Turma, isso não é tão importante: eles possuem uma ligação profunda entre si. Sejam por estudarem na mesma classe, terem encontrado juntos um local onde criaram uma espécie de _Santuário_ onde poderiam se divertir ou estarem de algum modo relacionados por serem todos da mesma igreja ou clube, ou mesmo por seus pais se conhecerem, eles desenvolveram entre si uma ligação aprofundada. Muitas vezes, apenas os integrantes da Turma se entendem bem, e eles entendem que todos se conhecem bem, ou ao menos bem o bastante para que os desentendimentos e desavenças não levem ao fim da Turma.

E não são poucos que tentam isso: existem Adultos que não aceitam quando você diz que seus amigos são mais importantes que a ordem da sala; existem Valentões que acham que resolvem todos os problemas os surrando até que eles deixem de ser problemas; existem garotos Mandões que acreditam que podem ter o que querem quando querem, e que a Turma é algo que ele quer AGORA para seu bel-prazer; existem os garotos Esquisitos, que soam tão assustadores que não conseguem interagir com nada e nem com ninguém (ao menos enquanto ninguém procura entender suas esquisitices); e existem todo o tipo de situações que podem colocar em risco a Turma, seja individualmente ou como um grupo.

Mas para todos da Turma, a Turma é um local onde eles podem se abrir sem maiores riscos. Claro que as travessuras do Travesso podem incomodar o Certinho, e o Geninho e o Atleta podem se bicarem para descobrir o que é mais legal, se um jogo de baseball ou uma boa tarde na biblioteca lendo sobre os romanos. Mas mesmo nesses casos, a Turma como um todo tende a levar a situação para algo mais simples, mais interessantes para todos, de modo que todos possam aproveitar bem a vida. Claro que isso pode implicar em o Atleta indo à biblioteca (que ele acha chata), ou o Certinho participando de uma travessura do Travesso (mesmo sabendo que isso vai colocá-los em encrenca). Mas o fundamental é que a Turma existe como ___Um por todos e Todos por um.___.

Como sugestão, Narrador, faça com que cada personagem contribua com um Aspecto para a Turma. Esses _Aspectos da Turma_ poderão ser invocado por qualquer jogador a qualquer momento. Entretanto, uma vez que eles são compartilhados por ___TODOS___, uma Forçada desse Aspecto afetará todos os personagens. 

_Aspectos da Turma_ podem representar:

+ Turmas ou garotos Rivais
	+ _A Turma da Rua de Cima é um bando de chatos_
	+ _O McHog é um grande problema!_
+ Pessoas que eles admiram ou temem
	+ _O Senhor Goatzeit nos dá medo!_
	+ _Os sorvetes da senhora Hecket são demais!_
+ Locais que funcionam como ponto de encontro, "bases secretas" ou qualquer coisa similar
	+ _A Doceria da Senhora Hecket é um bom lugar para ficar!_
	+ _O Velho Carroção perto da propriedade McBaalian é nossa base secreta!_


